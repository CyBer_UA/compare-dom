(function(undef) {

    /**
     *
     * Функция сравнение двух дом деревьев
     *
     * @param {HTMLElement} beforeElem исходный элемент
     * @param {HTMLElement} afterElem измененный элемент
     * @returns {Array.<Object>}

     *
     * */

	function diff ( beforeElem, afterElem ) {

     return  compare( beforeElem, afterElem );
	};


    /**
     * Функция сравнение двух дом деревьев
     *
     * @param {HTMLElement} beforeElem исходный элемент
     * @param {HTMLElement} afterElem измененный элемент
     * @param {boolean =} [isSplit = false] если true, то возвращает объект, с массивами added, removed, changed.
     * @return {( Array.<Object> | Object.<Array> )}
     *
     * */

    function compare ( beforeElem, afterElem, isSplit ) {

        var after,
            leng = getMaxLength( beforeElem, afterElem ),
            before,
            childCompare,
            added = [],
            removed = [],
            changed = [],
            res = [];

        if( !isSplit ) {

            textChanged( beforeElem, afterElem );
        }


        for( var i = 0; i < leng; i++ ) {

            before =  beforeElem.children[i];
            after = afterElem.children[i];

            if ( before === undef ) {

                push( "added", after );
                continue;
            }

            if( after === undef ) {
                push( "removed", before );
                continue;
            }


            if( before.tagName !== after.tagName ) {

                push( "added", after );
                push( "removed", before );

                continue;
            }


            if( getMaxLength ( before, after ) > 0 ) {

                childCompare = compare( before, after, true );

                if( isChanged( childCompare ) ) {

                    pushChanged( {
                        beforeElement: before,
                        afterElement: after,
                        added: childCompare.added,
                        removed: childCompare.removed,
                        changed: childCompare.changed
                    } );

                    continue;

                }

            }


            textChanged( before, after );
        };


        /**
         * Сравнивает текст 2х Element
         *
         * @param {HTMLElement} before
         * @param {HTMLElement} after
         *
         * */

        function textChanged ( before, after ) {

            var afterText = getText( after ),
                beforeText = getText( before );


            // изменен текст

            if( beforeText !== afterText ) {

                pushChanged( {
                    beforeElement: before,
                    afterElement: after,
                    beforeText: beforeText,
                    afterText: afterText
                });

            };
        }


        /**
         * Добавляет объект в массив итоговый массив
         *
         * @param {string} type
         * @param {HTMLElement} elem
         *
         * */

        function push (type, elem) {

            if(isSplit) {

                if(type === "added")
                    added.push(elem);

                if(type === "removed")
                    removed.push(elem);

                return;
            }


            res.push({
                type: type,
                element: elem
            });
        };

        /**
         * Добавляет объект "changed" в итоговый массив
         *
         * @param {object} obj
         *
         * */

        function pushChanged ( obj ) {

            obj.type = "changed";
            obj.html = obj.afterElement.innerHTML;

            if( isSplit ) {

                changed.push ( obj );

            };

            res.push ( obj );

        };


        return !isSplit ? res : {
            added: added,
            removed: removed,
            changed: changed
        };
    }

    /**
     * Возвращает максимальное кол-во "детей" для 2х элементов
     *
     * @param  {HTMLElement} fElem
     * @param {HTMLElement} sElem
     * @return {number}
     *
     * */

    function getMaxLength (fElem, sElem) {

        return Math.max(fElem.children.length, sElem.children.length);
    }


    /**
     * Проверка на наличие элементов в массивах объекта возвращенного diff
     *
     * @param {object} obj объект возвращенный diff
     *
     * */

    function isChanged ( obj ) {

        return obj.removed.length > 0 ||
            obj.changed.length > 0 ||
            obj.added.length > 0;
    }



    /**
     * Возвращает текст из дочерних текстовых элементов переданного элемента
     *
     * @param {HTMLElement} elem
     * @return {string}
     *
     * */

    function getText ( elem ) {

        var nodes = elem.childNodes,
            text = "";

        for (var i = 0 ; i < nodes.length; i++) {

            if( nodes[i].nodeType !== 3 )
                continue;

            text += nodes[i].nodeValue.trim();
        }

        return text;

    }


    window.diff = diff;

}( ));


    /***
     * Подсветка изменненых элементов
     *
     * @param { Array } selectObjects массив элементов возвращенных diff
     *
     */

    function select ( selectObjects ) {

        selectObjects.forEach( wrap );

        function wrap ( obj )  {
            select( obj.type , obj.element, obj );
        }

        function select ( type, elem, obj ) {
            var  border = function ( elem, style ){
                elem.style.border = style;
            }

            switch  ( type ) {
                case "added" :

                    border( elem, "1px solid green" ) ;

                    break;

                case "removed" :

                    border( elem, "1px solid red" ) ;

                    break;

                case "changed" :

                    if( typeof obj.beforeText  !== "string" ) {

                        obj.added.forEach( select.bind(this, "added") );
                        obj.removed.forEach( select.bind(this, "removed") );
                        obj.changed.forEach( wrap );
                    }

                    border( obj.beforeElement, "3px solid purple" );
                    border( obj.afterElement, "3px solid purple" );

                    break;
            }

        }

    }




